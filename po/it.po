# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Pierluigi Mario <pierluigimariomail@gmail.com>, 2023
# Dede Carli <dede.carli.drums@gmail.com>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-02-28 17:32+0200\n"
"PO-Revision-Date: 2023-02-28 15:33+0000\n"
"Last-Translator: Dede Carli <dede.carli.drums@gmail.com>, 2023\n"
"Language-Team: Italian (https://www.transifex.com/anticapitalista/teams/10162/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: trash-manager:5
msgid "Trash can manager"
msgstr "Gestore cestino"

#: trash-manager:6
msgid "  Are you sure you want to empty the trash can?  "
msgstr "Sei sicuro di voler svuotare il cestino?"

#: trash-manager:7
msgid "Empty trash"
msgstr "Svuota cestino"

#: trash-manager:8
msgid "Empty Trash"
msgstr "Svuota Cestino"

#: trash-manager:9
msgid "\\n Your trash can is currently using "
msgstr "\\n Il tuo cestino sta attualmente usando"

#: trash-manager:10
msgid " of your disk space.\\n Free space in your home folder is:"
msgstr "del tuo spazio su disco.\\n Lo spazio libero nella tua cartella home è:"

#: trash-manager:11
#, sh-format
msgid ""
"\\n \\n Note: you can use your file manager to simply drag file(s) or "
"folder(s) from the Trash Can back to where you want them,\\n or do it by "
"manually selecting their number from a list, using the button "
"'$empty_trash_button' \\n"
msgstr ""
"\\n \\n Nota: puoi usare il tuo file manager per trascinare semplicemente "
"il/i file o la/e cartella/e dal cestino a dove vuoi,\\n o farlo selezionando"
" manualmente il loro numero da un elenco, usando il pulsante "
"'$empty_trash_button' \\n"

#: trash-manager:12
msgid "Manually restore a file from trash"
msgstr "Ripristina manualmente un file dal cestino"

#: trash-manager:13
msgid " Done "
msgstr "Fatto"

#: trash-manager:14
msgid "Select file to undelete"
msgstr "Seleziona il file da ripristinare"

#: trash-manager:15
msgid ""
"Error. Something went wrong. \\n Please check if you entered a valid "
"selection."
msgstr ""
"Errore. Qualcosa è andato storto. \\n Controlla se hai inserito una "
"selezione valida."
