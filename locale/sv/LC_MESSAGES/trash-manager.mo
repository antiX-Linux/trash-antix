��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  3   �     
  =        N     _  O   p  /   �     �           "    C           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Henry Oquist <henryoquist@nomalm.se>, 2023
Language-Team: Swedish (https://www.transifex.com/anticapitalista/teams/10162/sv/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sv
Plural-Forms: nplurals=2; plural=(n != 1);
 Är du säker på att du vill tömma papperskorgen? Klart  av ditt diskutrymme.\n Ledigt utrymme i din hemkatalog är:: Töm Papperskorg Töm papperskorg Fel. Något gick fel. \n Var vänlig kontrollera att du gjorde ett giltigt val. Återställ en fil manuellt från papperskorgen Välj fil att återställa Papperskorgs-hanterare \n Din papperskorg använder nu  \n \n Notera: du kan använda din filhanterare för att helt enkelt dra fil(er) eller katalog(er) från papperskorgen tillbaka till där du vill ha dom,\n eller gör det genom att manuellt välja deras nummer från en lista, genom att använda knappen '$empty_trash_button' \n 