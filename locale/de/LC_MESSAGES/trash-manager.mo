��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  b  Q  9   �     �  <   �     4     F  [   X  6   �  &   �       #   $  "  H           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: aer, 2023
Language-Team: German (https://app.transifex.com/anticapitalista/teams/10162/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
 Sind sie sicher, dass sie den Papierkorb leeren möchten? Erledigt Ihres Speicherplatzes.\n In Ihrem home-Verzeichnis ist frei: Papierkorb leeren Papierkorb leeren Fehler. Etwas lief falsch. \n Bitte überprüfen, ob eine gültige Auswahl getroffen wurde. Eine Datei aus dem Papierkorb manuell wiederherstellen Zu wiederherstellende Datei auswählen Papierkorbmanager \n Ihr Papierkorb verwendet derzeit \n \n Hinweis: Sie können ihre Dateiverwaltung verwenden, um Dateien oder Ordner einfach aus dem Papierkorb dorthin zu ziehen, wo sie sie haben möchten, \n oder sie können dies tun, indem sie sie manuell aus einer Liste auswählen und die Schaltfläche '$empty_trash_button' verwenden \n 