��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  -        2  A   8     z     �  X   �  /   �  )   &     P  (   f  �   �           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Manuel <senpai99@hotmail.com>, 2023
Language-Team: Spanish (Spain) (https://www.transifex.com/anticapitalista/teams/10162/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 ¿Está seguro que quiere vaciar la papelera? Hecho de su espacio en disco.\n El espacio libre en su carpeta home es: Vaciar papelera Vaciar la papelera Error. Algo ha ido mal. \n or favor, compruebe si ha introducido una selección válida. Restaurar manualmente un archivo de la papelera Seleccione el archivo que desea recuperar Gestor de la papelera \n Su papelera está usando actualmente  \n \n Nota: puede usar su gestor de archivos para simplemente arrastrar archivo(s) o carpeta(s) desde la Papelera hasta donde desee,\n o hacerlo seleccionando manualmente su número de una lista, usando el botón '$empty_trash_button' \n 