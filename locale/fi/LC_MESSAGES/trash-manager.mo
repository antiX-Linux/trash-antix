��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  n  Q  2   �     �  =   �     8     L  G   `  $   �      �     �  +   	     5           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Risto Pärkkä, 2023
Language-Team: Finnish (https://www.transifex.com/anticapitalista/teams/10162/fi/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fi
Plural-Forms: nplurals=2; plural=(n != 1);
 Oletko varma, että haluat tyhjentää roskakorin? Valmis kiintolevysi tilasta. .\n  Vapaa tila kotihakemistossasi on: Tyhjennä roskakori Tyhjennä roskakori Virhe. Jokin meni pieleen.  \n Tarkista että valitsit oikean valinnan. Palauta tiedosto käsin roskakorista Valitse tiedosto palautettavaksi Roskakorin hallintaohjelma \n Roskakorisi käyttää tällä hetkellä \n \n Huomautus: voit käyttää tiedostonhallintaa yksinkertaisesti vetääksesi tiedosto(t) tai kansiot roskakorista takaisin haluamaasi paikkaan \n tai tehdä sen valitsemalla niiden numeron manuaalisesti luettelosta. painiketta '$empty_trash_button' \n 