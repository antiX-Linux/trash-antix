��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  p  Q  7   �     �  3         4     D  c   T  ,   �     �       ,     �   J           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Mehmet Akif 9oglu, 2023
Language-Team: Turkish (https://app.transifex.com/anticapitalista/teams/10162/tr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: tr
Plural-Forms: nplurals=2; plural=(n > 1);
 Çöp kutusunu boşaltmak istediğinizden emin misiniz? Tamam ini kullanıyor.\n Ana klasörünüzdeki boş alan: Çöpü Boşalt Çöpü boşalt Hata. Bir şeyler yanlış gitti. \n Lütfen geçerli bir seçim girip girmediğinizi kontrol edin. Dosyayı çöp kutusundan elle geri yükleme Geri alınacak dosyayı seçin Çöp kutusu yöneticisi \n Çöp kutunuz şu anda disk alanınızın \n \n Not: Dosya yöneticinizi kullanarak dosyaları veya klasörleri Çöp Kutusu'ndan istediğiniz yere geri sürükleyebilir veya \n bunu bir listeden numaralarını elle seçip  '$empty_trash_button' düğmesini \n kullanarak yapabilirsiniz. 