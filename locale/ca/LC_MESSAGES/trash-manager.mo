��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  ~  Q  )   �     �  P   �     O     b  c   t  ,   �          !  %   7  �   ]           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Eduard Selma <selma@tinet.cat>, 2023
Language-Team: Catalan (https://www.transifex.com/anticapitalista/teams/10162/ca/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ca
Plural-Forms: nplurals=2; plural=(n != 1);
 Esteu segurs de voler buidar la paperera? Fet del vostre espai de disc.\n L'espai disponible a la vostra carpeta d'usuari és: Buida la Paperera  Buida la Paperera Error. Alguna cosa ha anat malament. \n Si us plau, comproveu que heu entrat una selecció vàlida. Restaura manualment un fitxer de la paperera Trieu el fitxer a restaurar Gestor de la Paperera \n La vostra Paperera actualment usa  \n \n Nota: podeu usar el vostre Gestor de Fitxers per arrossegar fitxers o carpetes des de la Paperera a on vulgueu,\n o fer-ho manualment triant el seu número d'una llista, amb el botó '$empty_trash_button' \n 