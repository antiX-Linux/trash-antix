��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  _  Q  *   �     �  _   �     C     \  i   u  -   �  $        2  *   K  8  v           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Green, 2023
Language-Team: Japanese (https://www.transifex.com/anticapitalista/teams/10162/ja/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ja
Plural-Forms: nplurals=1; plural=0;
 本当にゴミ箱を空にしますか？ 完了  ディスクの空き容量について。\n ユーザーの home フォルダの空き容量: ゴミ箱を空にする ゴミ箱を空にする エラー。異常が発生しました。\n 正しく選択したかどうか確認してください。 ファイルをゴミ箱から手動で戻す 削除しないファイルを選択 ゴミ箱管理ツール \n 現在ゴミ箱を使用しています \n \n 注意。ファイルマネージャを使うと、ゴミ箱からファイルやフォルダをドラッグするだけで、アイテムをお好きな場所に戻せます。\n あるいは、'$empty_trash_button' ボタンを使って手動で一覧の番号から選択することもできます。\n 