��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  /   �  	      <   *     g     w  T   �  /   �          .  !   C  �   e           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Paulo C., 2023
Language-Team: Portuguese (https://www.transifex.com/anticapitalista/teams/10162/pt/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 Tem a certeza que deseja esvaziar a Reciclagem? Terminado de espaço em disco.\n O espaço livre na sua pasta home é: Esvaziar o Lixo Esvazia a Reciclagem Erro. Algo de errado aconteceu. \n Por favor verifique se fez uma seleção válida. Restaurar manualmente um ficheiro da Reciclagem Escolha ficheiro a restaurar Gestor da Reciclagem \n A Reciclagem está a utilizar  \n \n Nota: pode usar o gestor de janelas para simplesmente arrastar ficheiro(s) ou pasta(s)  da Reciclagem para o local pretendido, \n ou fazê-lo manualmente, escolhendo o número respetivo da lista, usando o botão '$empty_trash_button' \n 