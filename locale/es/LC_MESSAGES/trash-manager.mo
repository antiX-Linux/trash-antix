��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  &   �       A        M     ]  H   m  /   �     �       '     �   D           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Amigo, 2023
Language-Team: Spanish (https://www.transifex.com/anticapitalista/teams/10162/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 ¿Estás seguro de vaciar la papelera? Listo de su espacio en disco.\n El espacio libre en su carpeta home es: Vaciar papelera Vaciar papelera Error. Algo salió mal. \n Verifique si ingresó una selección válida. Restaurar manualmente un archivo de la papelera Seleccionar archivo a recuperar Gestor de la papelera \n Su papelera está usando actualmente \n \n Nota: puede usar su administrador de archivos para regresar archivos o carpetas desde la papelera a donde los desee simplemente arrastrándolos,\n o hacerlo seleccionando manualmente su número de una lista, usando el botón '$empty_trash_button' \n 