��          t      �         1        C  9   J     �     �  N   �  "   �          &  %   8  �  ^  H   X     �     �     0     P  �   l  D   �  =   D     �  P   �            	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Andrei Stepanov, 2023
Language-Team: Russian (https://app.transifex.com/anticapitalista/teams/10162/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
   Уверены, что хотите очистить корзину?    Готово   от вашего дискового пространства.\n Свободное место в домашней папке: Очистить корзину Пустая корзина Ошибка. Что-то пошло не так. \n Пожалуйста, проверьте, правильно ли вы ввели выбор. Вручную восстановить файл из корзины Выберите файл для восстановления Менеджер корзины \n Ваша корзина в данный момент используется 