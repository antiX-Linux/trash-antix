��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  2     
   4  P   ?     �     �  `   �  5     %   M     s  )   �  @  �           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Wallon Wallon, 2023
Language-Team: French (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/fr_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr_BE
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
   Êtes-vous sûr de vouloir vider la corbeille?    Terminé   de votre espace disque.\n L’espace libre dans votre dossier personnel est de: Vider la corbeille Vider la corbeille Erreur. Un problème est survenu. \n Vérifiez si la sélection a été correctement réalisée. Restaurer manuellement un fichier depuis la corbeille Sélectionner le fichier à restaurer Gestionnaire de corbeille \n Votre corbeille contient actuellement  \n \n Remarque: vous pouvez utiliser votre gestionnaire de fichiers pour faire simplement glisser le(s) fichier(s) ou le(s) dossier(s) de la corbeille vers l’endroit où vous voulez les placer,\n ou le faire en sélectionnant manuellement leur numéro dans une liste, à l’aide du bouton « $empty_trash_button » \n 