��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  #   �        *        2     C  ?   T  0   �     �     �     �  �              	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: heskjestad <cato@heskjestad.xyz>, 2023
Language-Team: Norwegian Bokmål (https://www.transifex.com/anticapitalista/teams/10162/nb/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nb
Plural-Forms: nplurals=2; plural=(n != 1);
 Vil du virkelig tømme papirkurven? Ferdig diskplass.\n Ledig plass i hjemmemappa er: Tøm papirkurven Tøm papirkurven Feil. Noe gikk galt. \n Kontroller om du gjorde et gyldig valg. Manuell gjenoppretting av ei fil fra papirkurven Velg fil som skal gjenopprettes Behandle papirkurven \n Papirkurven bruker nå \n \n Legg merke til at man kan dra filer eller mapper i filbehandleren fra papirkurven til der man vil ha dem,\n eller gjøre det manuelt ved å velge dem i ei liste og bruke knappen «$empty_trash_button»\n 