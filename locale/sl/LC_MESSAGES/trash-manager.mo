��          |      �          1   !     S  9   Z     �     �  N   �  "   �          6  %   H  �   n  �  Q  0        >  /   G     w     �  ;   �  "   �     �          !  �   A           	   
                                               Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Error. Something went wrong. \n Please check if you entered a valid selection. Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  \n \n Note: you can use your file manager to simply drag file(s) or folder(s) from the Trash Can back to where you want them,\n or do it by manually selecting their number from a list, using the button '$empty_trash_button' \n Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2023
Language-Team: Slovenian (https://www.transifex.com/anticapitalista/teams/10162/sl/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: sl
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 Ali ste prepričani, da želite izprazniti koš? Končano prostora na disku. \nV domači mapi je prosto:  Izprazni koš Izprazni koš Napaka. Nekaj je bilo narobe.\nPreverite pravilnost izbora. Ročna obnovitev datoteke iz koša Izberite datoteko za obnovo Upravljalnik koša \nVaš koš trenutno uporablja  \|n\n Opomba: s pomočjo upravljalnika datotek je mogoče preprosto povleči datoteke ali mape iz koša na mesto kamor želite,\n lahko pa ročno izberete njihove številke s seznama ob uporabi gumba  '$empty_trash_button' \n 