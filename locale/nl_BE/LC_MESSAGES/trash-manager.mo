��    
      l      �       �   1   �      #  9   *     d     p  "   |     �     �  %   �    �  /   o     �  6   �     �     �  /   �  6   .     e  )   {                      
      	                   Are you sure you want to empty the trash can?    Done   of your disk space.\n Free space in your home folder is: Empty Trash Empty trash Manually restore a file from trash Select file to undelete Trash can manager \n Your trash can is currently using  Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-02-28 15:33+0000
Last-Translator: Vanhoorne Michael, 2023
Language-Team: Dutch (Belgium) (https://app.transifex.com/anticapitalista/teams/10162/nl_BE/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: nl_BE
Plural-Forms: nplurals=2; plural=(n != 1);
 Weet je zeker dat je de prullenmand wilt legen? Klaar van uw schijfruimte.\n Vrije ruimte in uw thuismap is: Prullenbak Legen Prullenbak legen Herstel handmatig een bestand uit de prullenbak Selecteer bestand om de verwijdering ongedaan te maken Prullenmand beheerder \n Je prullenmand is momenteel in gebruik 